# Our top priority is [SecureMail](https://www.github.com/JohnLockwood/SecureMail).
Here is **[the current bug](https://github.com/JohnLockwood/SecureMail/issues?labels=Current&page=1&state=open)**.
And here is the [SecureMail Documentation](https://github.com/JohnLockwood/SecureMail/wiki).
Your notes on what to do after vacation are on the [hours page](https://github.com/JohnLockwood/SecureMail/wiki/hours).
We're making excellent progress (take a bow).
This will be our first SasS site.  Escrowmaker may be a spinoff of this with Realtor-branding.


------------------------------------------------------------------------------------------------------

## Lower Priority Clutter
* Should we spin off a membership site "core"? It needs to be easy to set up sties based on core authentication etc of Owner + Admin + Members.  This is a high profit model.  See for example 
the [RailsKits](http://railskits.com/saas/) guy, who's selling such a core.  This should be based (loosely?) on [aprojectx](https://github.com/JohnLockwood/aprojectx).
Or for more on authorization see this StackOverflow post on [Rails SAS](http://stackoverflow.com/questions/2252213/recommended-rails-plugins-for-software-as-a-service-app).
NOTE:  This sill slow down above task.
* Escrowmail and Java Starter Projects.  What's up with using Java?  Are you nuts?  10% [https://github.com/JohnLockwood/StarterProjects](https://github.com/JohnLockwood/StarterProjects)

## Welcome to Root

The [Root](https://www.github.com/JohnLockwood/Root) project is **the single source** (DRY, anyone?) for global goals, ideas and [issues](https://www.github.com/JohnLockwood/Root/issues).

Use alias "root" to cd to this directory and load this file.

All other roads should lead here too.





